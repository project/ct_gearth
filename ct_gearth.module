<?php

/**
 * @file
 * Enables keeping an easily and regularly updated web page or a ct_gearth.
 */

/**
 * Implementation of hook_node_info().
 */
function ct_gearth_node_info() {
  return array(
  'ct_gearth' => array(
  'name' => t('Kml for Google earth'),
  'module' => 'ct_gearth',
  'description' => t('Kml file for Google earth.'),
  )
  );
}


function ct_gearth_menu() {
  $items['admin/settings/gearth'] = array(
    'title' => 'Google Earth content type',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ct_gearth_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implementation of hook_perm().
 */
function ct_gearth_perm() {
  return array('create Kml for Google earth entries', 'delete own Kml for Google earth entries', 'delete any Kml for Google earth entry', 'edit own Kml for Google earth entries', 'edit any Kml for Google earth entry');
}

/**
 * Implementation of hook_access().
 */
function ct_gearth_access($op, $node, $account) {
  switch ($op) {
    case 'create':
    // Anonymous users cannot post even if they have the permission.
      return user_access('create Kml for Google earth entries', $account) && $account->uid ? TRUE : NULL;
    case 'update':
      return user_access('edit any Kml for Google earth entry', $account) || (user_access('edit own Kml for Google earth entries', $account) && ($node->uid == $account->uid)) ? TRUE : NULL;
    case 'delete':
      return user_access('delete any Kml for Google earth entry', $account) || (user_access('delete own Kml for Google earth entries', $account) && ($node->uid == $account->uid)) ? TRUE : NULL;
  }
}

/**
 * Implementation of hook_user().
 */
function ct_gearth_user($type, &$edit, &$user) {
  if ($type == 'view' && user_access('create Kml for Google earth entries', $user)) {
    $user->content['summary']['ct_gearth'] =  array(
        '#type' => 'user_profile_item',
        '#title' => t('Kml for Google earth'),
        // l() escapes the attributes, so we should not escape !username here.
        '#value' => l(t('View recent Kml files for Google earth entries'), "ct_gearth/$user->uid", array('attributes' => array('title' => t("Read !username's latest Kml files for Google earth entries.", array('!username' => $user->name))))),
        '#attributes' => array('class' => 'ct_gearth'),
    );
  }
}

/**
 * Implementation of hook_form().
 */
function ct_gearth_form(&$node) {
  global $nid;
  $iid = isset($_GET['iid']) ? (int)$_GET['iid'] : 0;
  $type = node_get_types('type', $node);


  if (empty($node->body)) {
  // If the user clicked a "ct_gearth it" link, we load the data from the
  // database and quote it in the ct_gearth.
    if ($nid && $ct_gearth = node_load($nid)) {
      $node->body = '<em>'. $ct_gearth->body .'</em> ['. l($ct_gearth->name, "node/$nid") .']';
    }

    if ($iid && $item = db_fetch_object(db_query('SELECT i.*, f.title as ftitle, f.link as flink FROM {aggregator_item} i, {aggregator_feed} f WHERE i.iid = %d AND i.fid = f.fid', $iid))) {
      $node->title = $item->title;
      // Note: $item->description has been validated on aggregation.
      $node->body = '<a href="'. check_url($item->link) .'">'. check_plain($item->title) .'</a> - <em>'. $item->description .'</em> [<a href="'. check_url($item->flink) .'">'. check_plain($item->ftitle) ."</a>]\n";
    }
  }

  $form['title'] = array('#type' => 'textfield', '#title' => check_plain($type->title_label), '#required' => TRUE, '#default_value' => !empty($node->title) ? $node->title : NULL, '#weight' => -5);
  $form['body_field'] = node_body_field($node, $type->body_label, $type->min_word_count);

  // Now we define the form elements specific to our node type.
  $form['additional_fields'] = array(
      '#type' => 'fieldset',
      '#title' => t('Additional Map Settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );
  $form['additional_fields']['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#default_value' => isset($node->width) ? $node->width : 400,
      '#size' => 10,
      '#maxlength' => 10,
  );
  $form['additional_fields']['height'] = array(
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#default_value' => isset($node->height) ? $node->height : 400,
      '#size' => 10,
      '#maxlength' => 10,
  );
  $form['additional_fields']['borders'] = array(
      '#type' => 'checkbox',
      '#title' => t('Borders'),
      '#default_value' => isset($node->borders) ? $node->borders : 0,
  );
  $form['additional_fields']['roads'] = array(
      '#type' => 'checkbox',
      '#title' => t('Roads'),
      '#default_value' => isset($node->roads) ? $node->roads : 0,
  );
  $form['additional_fields']['terrain'] = array(
      '#type' => 'checkbox',
      '#title' => t('Terrain'),
      '#default_value' => isset($node->terrain) ? $node->terrain : 0,
  );
  $form['additional_fields']['buildings'] = array(
      '#type' => 'checkbox',
      '#title' => t('3d buildings'),
      '#default_value' => isset($node->buildings) ? $node->buildings : 0,
  );
  $form['additional_fields']['grid'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show Grid'),
      '#default_value' => isset($node->grid) ? $node->grid : 0,
  );
  $form['additional_fields']['statusbar'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show statusbar'),
      '#default_value' => isset($node->statusbar) ? $node->statusbar : 0,
  );
  $form['additional_fields']['scale'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show scale'),
      '#default_value' => isset($node->scale) ? $node->scale : 0,
  );
  $form['additional_fields']['overview'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show overview'),
      '#default_value' => isset($node->overview) ? $node->overview : 0,
  );
/*   $form['display'] = array(
    '#type' => 'select',
    '#default_value' => isset($node->display) ? $node->display : 1,
	'#options' => array(
      '1' => t('Earth'),
      '2' => t('Moon'),
      '3' => t('Mars'),
      '4' => t('Sky'),
    ),
  ); */
  return $form;
}


/**
 * Implementation of hook_insert().
 *
 * As a new node is being inserted into the database, we need to do our own
 * database inserts.
 */
function ct_gearth_insert($node) {
  db_query("INSERT INTO {content_type_ct_gearth} (vid, nid, width, height, borders, roads, terrain, buildings, grid, statusbar, scale, overview) VALUES (%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d)", $node->vid, $node->nid, $node->width, $node->height, $node->borders, $node->roads, $node->terrain, $node->buildings, $node->grid, $node->statusbar, $node->scale, $node->overview);
}

/**
 * Implementation of hook_update().
 *
 * As an existing node is being updated in the database, we need to do our own
 * database updates.
 */
function ct_gearth_update($node) {
// if this is a new node or we're adding a new revision,
  if ($node->revision) {
    node_example_insert($node);
  }
  else {
    db_query("UPDATE {content_type_ct_gearth} SET width = '%d', height = %d, borders = %d, roads = %d, terrain = %d, buildings = %d, grid = %d, statusbar = %d, scale = %d, overview = %d WHERE vid = %d", $node->width, $node->height, $node->borders, $node->roads, $node->terrain, $node->buildings, $node->grid, $node->statusbar, $node->scale, $node->overview, $node->vid);
  }
}

/**
 * Implementation of hook_load().
 *
 * Now that we've defined how to manage the node data in the database, we
 * need to tell Drupal how to get the node back out. This hook is called
 * every time a node is loaded, and allows us to do some loading of our own.
 */
function ct_gearth_load($node) {
  $additions = db_fetch_object(db_query('SELECT width, height, borders, roads, terrain, buildings, grid, statusbar, scale, overview FROM {content_type_ct_gearth} WHERE vid = %d', $node->vid));
  return $additions;
}

/**
 * Implementation of hook_delete().
 *
 * When a node is deleted, we need to remove all related records from out table.
 */
function ct_gearth_delete($node) {
// Notice that we're matching all revision, by using the node's nid.
  db_query('DELETE FROM {content_type_ct_gearth} WHERE nid = %d', $node->nid);
  db_query('DELETE FROM {node} WHERE nid = %d', $node->nid);
}

/**
 * Implementation of hook_validate().
 *
 * Our "quantity" field requires a number to be entered. This hook lets
 * us ensure that the user entered an appropriate value before we try
 * inserting anything into the database.
 *
 * Errors should be signaled with form_set_error().
 */
function ct_gearth_validate(&$node) {
  if ($node->width) {
    if (!is_numeric($node->width)) {
      form_set_error('width', t('The width must be a number.'));
    }
  }
  else
  {
    // Default width value
    $node->width = 400;
  }
  if ($node->heigh) {
    if (!is_numeric($node->width)) {
      form_set_error('heigh', t('The heigh must be a number.'));
    }
  }
  else
  {
    // Default heigh value
    $node->heigh = 400;
  }
}

/**
 * Implementation of hook_view().
 */
function ct_gearth_view($node, $teaser = FALSE, $page = FALSE) {
  if ($page) {
  // Breadcrumb navigation. l() escapes the title, so we should not escape !name.
    drupal_set_breadcrumb(array(l(t('Home'), NULL), l(t('Kml for Google earth'), 'ct_gearth')));
  }
  return node_prepare($node, $teaser);
}


/**
 * Implementation of hook_menu().
 */
/* function ct_gearth_menu() {
  $items['ct_gearth'] = array(
    'title' => 'ct_gearths',
    'page callback' => 'ct_gearth_page_last',
    'access arguments' => array('access content'),
    'type' => MENU_SUGGESTED_ITEM,
    'file' => 'ct_gearth.pages.inc',
  );  return $items;
} */

/**
 * Access callback for user ct_gearth pages.
 */
function ct_gearth_page_user_access($account) {
// The visitor must be able to access the site's content.
// For a ct_gearth to 'exist' the user must either be able to
// create new ct_gearth entries, or it must have existing posts.
  return $account->uid && user_access('access content') && (user_access('create Kml for Google earth entries', $account) || _ct_gearth_post_exists($account));
}

function ct_gearth_theme_registry_alter(&$theme_registry) {
  $template = 'node'; // example: page
  // dsm ($theme_registry);
  $originalpath = array_shift($theme_registry[$template]['theme paths']);
  // Get the path to this module
  $modulepath = drupal_get_path('module', 'ct_gearth');
  // Stick the original path with the module path back on top
  array_unshift($theme_registry[$template]['theme paths'], $originalpath, $modulepath);
}

/**
* GVS settings form
*/
function ct_gearth_admin_settings(&$form_state) {
  // if Google map api key exists use it
  if (!module_exists('keys_api')) {
    $ct_gearth_api_key = variable_get('timelinemap_api_key', '');
    if ($ct_gearth_api_key == '' && module_exists('gmap'))
        $ct_gearth_api_key = variable_get('googlemap_api_key', ''); 
    if ($ct_gearth_api_key == '' && module_exists('tagmap'))
        $ct_gearth_api_key = variable_get('tagmap_api_key', ''); 
    if ($ct_gearth_api_key == '' && module_exists('timelinemap_api_key'))
        $ct_gearth_api_key = variable_get('timelinemap_api_key', ''); 
    if ($ct_gearth_api_key == '' && module_exists('gvs'))
        $ct_gearth_api_key = variable_get('gvs_api_key', ''); 
            
    $form['ct_gearth_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Google Maps API Key'),
      '#default_value' => $ct_gearth_api_key,
      '#size' => 50,
      '#maxlength' => 255,
      '#description' => t('Your personal Googlemaps API key. You must get this for each separate website at <a href="http://www.google.com/apis/maps/">Google Map API website</a>.'),
    );
   }
  else {
    $form['ct_gearth_api_key'] = array(
      '#type' => 'item',
      '#title' => t('Google Maps API Key'),
      '#description' => t('Your personal Googlemaps API key.  You must get this for each separate website at <a href="http://www.google.com/apis/maps/">Google Map API website</a>.'),
      '#value' => t("Managed by <a href='@url'>keys api</a>.", array('@url' => url('admin/settings/keys'))),
    );
  }
  return system_settings_form($form);
}
