<?php

/**
 * @file
 * Enables keeping an easily and regularly updated web page or a ct_gearth.
 */
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> clear-block">
  <?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <?php endif; ?>
  <?php if ($submitted || $terms): ?>
  <div class="meta">
      <?php if ($submitted): ?>
    <div class="submitted"><?php print $submitted ?></div>
      <?php endif; ?>

      <?php if ($terms): ?>
    <div class="terms"><?php print $terms ?></div>
      <?php endif;?>
  </div>
  <?php endif; ?>

  <div class="content clear-block">
    <?php
    $kml_path=array();
    foreach ($node->files as $key) {
      $kml_path[]=$key->filepath;
    }

    if (count($kml_path)>0) {
    /**
     *  Insert Google map key
     */

      $api_key = variable_get('ct_gearth_api_key', False);
      if ($api_key) {
        $gvs_gmap_gapi = "http://www.google.com/jsapi?key=" . $api_key;
        drupal_set_html_head('<script type="text/javascript" src="' . $gvs_gmap_gapi . '"></script>');
      }
      else
        drupal_set_message('Enter Google maps key in Gmap settings');

      /**
       *  Generate Google earth API calls
       */

      $inlinet =
          'google.load("earth", "1"); var ge_' . $node->nid . ' = null; var networkLink_' . $node->nid . ';

function init_' . $node->nid . '(){
    google.earth.createInstance("map_' . $node->nid . '", initCallback_' . $node->nid . ', failureCallback_' . $node->nid . ');
};
function initCallback_' . $node->nid . '(instance){
    ge_' . $node->nid . ' = instance;
    ge_' . $node->nid . '.getWindow().setVisibility(true);
    ge_' . $node->nid . '.getNavigationControl().setVisibility(ge_' . $node->nid . '.VISIBILITY_AUTO);';

      if ($node->roads) $inlinet .= 'ge_' . $node->nid . '.getLayerRoot().enableLayerById(ge_' . $node->nid . '.LAYER_ROADS, true);';
      else $inlinet .= 'ge_' . $node->nid . '.getLayerRoot().enableLayerById(ge_' . $node->nid . '.LAYER_ROADS, false);';
      if ($node->borders) $inlinet .= 'ge_' . $node->nid . '.getLayerRoot().enableLayerById(ge_' . $node->nid . '.LAYER_BORDERS, true);';
      else $inlinet .= 'ge_' . $node->nid . '.getLayerRoot().enableLayerById(ge_' . $node->nid . '.LAYER_BORDERS, false);';
      if ($node->buildings) $inlinet .= 'ge_' . $node->nid . '.getLayerRoot().enableLayerById(ge_' . $node->nid . '.LAYER_BUILDINGS, true);';
      else $inlinet .= 'ge_' . $node->nid . '.getLayerRoot().enableLayerById(ge_' . $node->nid . '.LAYER_BUILDINGS, false);';
      if ($node->terrain) $inlinet .= 'var layerRoot = ge_' . $node->nid . '.getLayerRoot();var terrainLayer = layerRoot.getLayerById(ge_' . $node->nid . '.LAYER_TERRAIN); terrainLayer.setVisibility(true);';
      else $inlinet .= 'var layerRoot = ge_' . $node->nid . '.getLayerRoot();var terrainLayer = layerRoot.getLayerById(ge_' . $node->nid . '.LAYER_TERRAIN); terrainLayer.setVisibility(false);';
      if ($node->grid) $inlinet .= 'ge_' . $node->nid . '.getOptions().setGridVisibility(true);';
      else $inlinet .= 'ge_' . $node->nid . '.getOptions().setGridVisibility(false);';
      if ($node->statusbar) $inlinet .= 'ge_' . $node->nid . '.getOptions().setStatusBarVisibility(true);';
      else $inlinet .= 'ge_' . $node->nid . '.getOptions().setStatusBarVisibility(false);';
      if ($node->scale) $inlinet .= 'ge_' . $node->nid . '.getOptions().setScaleLegendVisibility(true);';
      else $inlinet .= 'ge_' . $node->nid . '.getOptions().setScaleLegendVisibility(false);';
      if ($node->overview) $inlinet .= 'ge_' . $node->nid . '.getOptions().setOverviewMapVisibility(true);';
      else $inlinet .= 'ge_' . $node->nid . '.getOptions().setOverviewMapVisibility(false);';

      $inlinet .=
          '   var link = ge_' . $node->nid . '.createLink("");
    link.setHref("' . file_create_url($kml_path[0]) . '");
    networkLink_' . $node->nid . ' = ge_' . $node->nid . '.createNetworkLink("");
    networkLink_' . $node->nid . '.set(link, true, true);
    ge_' . $node->nid . '.getFeatures().appendChild(networkLink_' . $node->nid . ');
}
function failureCallback_' . $node->nid . '(errorCode) {}
google.setOnLoadCallback(init_' . $node->nid . ');';
      if (count($kml_path)>1) {
        $inlinet .= '	function redrawKML(kml){
		ge_' . $node->nid . '.setBalloon(null);
		ge_' . $node->nid . '.getFeatures().removeChild(networkLink_' . $node->nid . ');

		var link = ge_' . $node->nid . '.createLink("");
		link.setHref(kml);
		networkLink_' . $node->nid . '.set(link, true, true);
		ge_' . $node->nid . '.getFeatures().appendChild(networkLink_' . $node->nid . ');
                }';
      }
      drupal_add_js($inlinet, 'inline');
      ?>
    <div id="map_<?php print $node->nid; ?>" style="height:<?php print $node->height; ?>px;width:<?php print $node->width; ?>px;"></div>
      <?php
      if (count($kml_path)>1) {
        echo '<select onchange="redrawKML(this.options[this.selectedIndex].value);">';
        foreach ($kml_path as $KML)
          echo '<option value="' . file_create_url($KML) . '">' . drupal_substr($KML, strrpos($KML, "/")+1) . '</option>';
        echo '</select>';
      }
      ?>
    <?php } print $content ?>
  </div>

  <?php
  if ($links) {
    print '<div class="node-links">'. $links .'</div>';
  }
  ?>

</div>
